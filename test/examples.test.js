const { Builder, By, Key, until } = require('selenium-webdriver');
const assert = require('assert');
const fs = require('fs');

const writeScreenshot = (data, name) => fs.writeFileSync(name, data, 'base64');

test('Should produce 35 as an output after multiplying 5 and 7', async () => {
  const driver = new Builder().forBrowser('chrome').build();
  await driver.get('http://www.anaesthetist.com/mnm/javascript/calc.htm');

  await (await driver.findElement({ name: 'seven' })).click();
  await (await driver.findElement({ name: 'mul' })).click();
  await (await driver.findElement({ name: 'five' })).click();
  await (await driver.findElement({ name: 'result' })).click();
  const result = await (
    await driver.findElement({ name: 'Display' })
  ).getAttribute('value');
  assert.strictEqual(result, '35');

  driver.takeScreenshot().then((data) => writeScreenshot(data, 'test1.png'));
  await driver.quit();
});

test('Should produce 6 as an output after multiplying 2 and 3', async () => {
  const driver = new Builder().forBrowser('chrome').build();
  await driver.get('http://www.anaesthetist.com/mnm/javascript/calc.htm');

  await (await driver.findElement({ name: 'two' })).click();
  await (await driver.findElement({ name: 'mul' })).click();
  await (await driver.findElement({ name: 'three' })).click();
  await (await driver.findElement({ name: 'result' })).click();
  const result = await (
    await driver.findElement({ name: 'Display' })
  ).getAttribute('value');
  assert.strictEqual(result, '6');

  driver.takeScreenshot().then((data) => writeScreenshot(data, 'test2.png'));
  await driver.quit();
});
